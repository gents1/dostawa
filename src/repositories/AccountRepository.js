import Account from '../models/Account'
import AbstractRepository from './AbstractRepository'

class AccountRespository extends AbstractRepository{
    constructor(){
        super(Account)
    }

}

export default  AccountRespository