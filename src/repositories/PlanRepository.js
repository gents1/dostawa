import Plan from '../models/Plan'
import AbstractRepository from './AbstractRepository'

class PlanRespository extends AbstractRepository{
    constructor(){
        super(Plan)
    }

}

export default  PlanRespository