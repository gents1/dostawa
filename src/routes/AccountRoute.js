import { Router } from 'express';
import AccountController from '../controllers/AccountController'

/* eslint-disable new-cap */
const routes = Router();
/* eslint-enable new-cap */

const controller = new AccountController()

/**
 * This function used for list or get by id.
 * @route GET /accounts/id?
 * @group Account
 * @param { integer } body.req.id
 * @returns { object } 200 - Return JSON results
 * @returns { Error }  401 - Invalid Login!
 */
routes.get('/:id?', controller.list);
/**
 * This function used for create register.
 * @route POST /accounts/
 * @group Account
 * @returns { object } 200 - Return JSON with success mesage
 * @returns { Error }  401 - Invalid Login!
 */
routes.post('/', controller.create);
/**
 * This function used for update register by id.
 * @route PUT /accounts/id
 * @group Account
 * @param { object } body.req
 * @returns { object } 200 - Return JSON result
 * @returns { Error }  401 - Invalid Login!
 */
routes.put('/:id', controller.update);
/**
 * This function used for list or get by id.
 * @route DELETE /accounts/id
 * @group Account
 * @param { object } body.body.required
 * @returns { object } 200 - Return JSON with success mesage
 * @returns { Error }  401 - Invalid Login!
 */
routes.delete('/:id', controller.delete);

export default routes;
