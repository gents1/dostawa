import AbstractController from './AbstractController'
import AccountRepository from '../repositories/AccountRepository'
import Account from '../models/Account'
import SequelizeDB from '../../lib/SequelizeDB'

const repository = new AccountRepository()

class AccountController extends AbstractController {
    constructor() {
        super()
    }

    list(req, res){
        
        super.list(req, res, repository)
    }

    create(req, res) {
        super.create(req, res, repository)
    }

    update(req, res) {
        super.update(req, res, repository)
    }

    delete(req, res) {
        super.delete(req, res, repository)
    }

    async verificaToken(token){
        return await Account.findOne({ 
            where: { 
                token: token 
            } 
        })
        .then( Account => { 
            return Account 
        })
        .catch( error => {
            console.log(error)
        })
    }

}

export default AccountController