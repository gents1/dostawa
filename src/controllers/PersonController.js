import AbstractController from './AbstractController'
import PersonRepository from '../repositories/PersonRepository'
import Person from '../models/Person'
import SequelizeDB from '../../lib/SequelizeDB'

const repository = new PersonRepository()

class PersonController extends AbstractController {
    constructor() {
        super()
    }

    list(req, res){
        super.list(req, res, repository)
    }

    create(req, res) {
        super.create(req, res, repository)
    }

    update(req, res) {
        console.log(req)
        super.update(req, res, repository)
    }

    delete(req, res) {
        super.delete(req, res, repository)
    }

    async verificaToken(token){
        return await Person.findOne({ 
            where: { 
                token: token 
            } 
        })
        .then( Person => { 
            return Person 
        })
        .catch( error => {
            console.log(error)
        })
    }

}

export default PersonController