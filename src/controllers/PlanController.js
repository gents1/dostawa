import AbstractController from './AbstractController'
import PlanRepository from '../repositories/PlanRepository'
import Plan from '../models/Plan'

const repository = new PlanRepository()

class PlanController extends AbstractController {
    constructor() {
        super()
    }

    list(req, res){
        super.list(req, res, repository)
    }

    create(req, res) {
        super.create(req, res, repository)
    }

    update(req, res) {
        super.update(req, res, repository)
    }

    delete(req, res) {
        super.delete(req, res, repository)
    }

}

export default PlanController